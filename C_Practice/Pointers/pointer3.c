#include <stdio.h>


int main()
{
  int m = 300;
  int* p_m = &m;
  
  double fx = 300.600006;
  double* p_fx = &fx;
  
  char cht = 'z';
  char* p_cht = &cht;
  
 
  // using the & operator

  printf("Address of m = %p.\n", &m);
  printf("Address of fx = %p.\n", &fx);
  printf("Address of cht = %p.\n", &cht);
  
  // using the & and * operator

  printf("Value at address of m = %d \n",*(&m));
  printf("Value at address of fx = %f\n",*(&fx));
  printf("Value at address of cht = %c\n",*(&cht));

  // using pointer variable only


 printf("The address of m = %p. \n", p_m);
 printf("The address of fx = %p. \n", p_fx);
 printf("The address of cht = %p. \n", p_cht);

  // using pointer operator

  printf("The value at the address of m = %d.\n", *(p_m));
  printf("The value at the address of fx = %f.\n", *(p_fx));
  printf("The value at the address of cht = %c.\n", *(p_cht));
 


  
  
}
