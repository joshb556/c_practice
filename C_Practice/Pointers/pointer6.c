// find the maximum number between two numbers input. This code assumes that the user has entered valid integers.

#include <stdio.h>

int compare_function(int* number_to_be_compared1, int* number_to_be_compared2);

int main()
{
  int input1, input2;
  // take user input
  printf("Please enter the first input.\n");
  scanf("%d", &input1);

  printf("Please enter the second input.\n");
  scanf("%d", &input2);
  

  //call the compare funciton and display the larger number  
  printf("The larger number is %d.\n", compare_function(&input1, &input2));

  return(0);
}


int compare_function(int* number_to_be_compared1, int* number_to_be_compared2)
{
  int larger_number;

  if( *number_to_be_compared1 < *number_to_be_compared2)
    {
      larger_number = *number_to_be_compared2;
    }
  else if(*number_to_be_compared1 == *number_to_be_compared2)
    {
      return 0;
    }
  else if (*number_to_be_compared1 > *number_to_be_compared2)
    {
      larger_number = *number_to_be_compared1;
    }

  return larger_number;
}
