/*
Practice for realloc
 */
#include <stdio.h>
#include <stdlib.h>

int main()
{
  

  int* p_int_array;
  int i;

  p_int_array = (int*)malloc(sizeof(int) * 2);
  *p_int_array = 1;
  *(p_int_array + 1) = 2;
  //*p_char_array = '\0';
  


  printf("The size of the array before realloc: %lu\n", sizeof(p_int_array)/sizeof(int*));

  p_int_array= realloc(p_int_array, sizeof(int*)*3);
  
  *(p_int_array + 2) = 3;

  printf("The size of the array after realloc: %lu\n", sizeof(p_int_array)/sizeof(int*));

  for(i = 0; i < 3; i++)
    {
      printf("%d\n", p_int_array[i]);

    }

  free(p_int_array);
  

  return 0;
  
}
