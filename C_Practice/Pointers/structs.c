/*
This is to practice using structs. The syntax of a struct is as follows:

to create:

typedef struct Address
{
char name[50];
char street[100];
char city[50]
char state[20]
int pin;

} Address;


Create an array of structs with 1 integer and initialize with random numbers.
print them out.

 */





#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct Data
{
  int data;

}Data;

void initialize_database(Data* database_entry, int elements);

int main()
{
  Data* pstruct_database;
  int elements, i;

  printf("How many elements will there be in the database? : ");
  scanf("%d", &elements);
  printf("\n");
  
  pstruct_database = (Data*)malloc(sizeof(Data*) * elements);

  initialize_database(pstruct_database, elements);

  for(i = 0; i < elements; i++)
    {
  printf("datapoint: %d | contents: %d\n", i, pstruct_database[i].data);
    }

  return 0;

}

void initialize_database(Data* database_entry, int elements)
{
  srand(time(NULL));
  
  int i;

  for(i = 0; i < elements; i++)
    {
      database_entry[i].data = rand();
    }

  

}


