/*
This is a practice to reverse the elements of an array.
 */


#include <stdio.h>
#include <stdlib.h>


void reverse_elements(int* array_to_reverse, int first_index, int last_index);
void initialize_array(int* array_to_initialize, int array_size);
void swap(int* array_to_swap,int swap_index1, int swap_index2);

int main()
{
  int* p_int_array;
  int first_index, last_index, array_size, i;
  first_index = 0;

  printf("Please enter the number of elements to be inserted into the array: ");
  scanf("%d", &array_size);

  p_int_array = (int*)malloc(sizeof(int) * array_size);
  if(p_int_array == NULL)
    {
      printf("Failed to allocate memory!\n");
      return(-1);
    }

  
  
  last_index=array_size-1;
  initialize_array(p_int_array, array_size);

  for(i = 0; i < array_size; i++)
    {
      printf("%d\n", p_int_array[i]);

    }
  
  reverse_elements(p_int_array, first_index, last_index);

  printf("Reverseing the elements!\n");

  for(i = 0; i < array_size; i++)
    {
      printf("%d\n", p_int_array[i]);

    }

  free(p_int_array);
  return 0;  
}

void initialize_array(int* array_to_initialize, int array_size)
{
  int i;

  for(i = 0; i < array_size; i++)
    {
      array_to_initialize[i] = i;
    }
  printf("Array initialized!\n");  
}

void swap(int* array_to_swap, int swap_index1, int swap_index2)
{
  int temp;
  temp = array_to_swap[swap_index1];
  array_to_swap[swap_index1] = array_to_swap[swap_index2];
  array_to_swap[swap_index2] = temp;  
}

void reverse_elements(int* array_to_reverse, int first_index, int last_index)
{
  int i;
  for(i = first_index; i <= last_index-1; i++)
    {

      
      swap(array_to_reverse, i, last_index);
      last_index--;

      
    }

}


