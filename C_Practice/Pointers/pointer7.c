// this code is to display all the different permutations of a 4 letter string "abcd" using pointers;
#include <stdio.h>

void print_string(char char_array[], int array_size);
void swap(char* char_to_swap_1, char* char_to_swap2);
void permute(char* p_string, int start_index, int end_index);


int main()
{
  //declarations;
  char my_string[25] = "abcd";
  int i,j,count;
  count = 0;
  printf("Original string: ");
  // count how big the string is
  for (j = 0; my_string[j] != '\0'; j++)
    {      
      count++;
    }
  // printf("%d", count);

  permute(my_string, 0, count-1);

  //printf("\n");
  //printf("%d characters. \n", count);

  

  return 0; 
  
}

//this function takes the string size and the the character array and prints it //out
void print_string(char char_array[], int array_size)
{
  int i;
  for(i = 0; i <= array_size; i++)
    {
      printf("%c", char_array[i]);
    }
  printf("\n");
  
}

// this function will swap two characters in an array.
void swap(char* char_to_swap1, char* char_to_swap2)
{
  char temp;
  temp = *char_to_swap2;
  *char_to_swap2 = *char_to_swap1;
 *char_to_swap1 = temp;  
}


// I had to look the algorithm for this, it is recursive
void permute(char* p_string, int start_index, int end_index)
{
  int i;
  if(start_index == end_index)
    {
      print_string(p_string, end_index);
    }
  else
    {
      for(i = start_index; i <= end_index; i++)
	{

	  swap((p_string + start_index), (p_string+i));
	  permute(p_string, start_index+1, end_index);
	  swap((p_string + start_index), (p_string+i));
	}

    }

}
