
// add two numbers using pointers. added error checking to make sure only integers were entered into the user input.

#include<stdio.h>


void clear_key(void)
{
char c;



scanf("%c", &c);
while (c != '\n')
{
scanf("%c", &c);
}
}

int main()
{

  int input1, input2, number_of_conversions, *p_input1, *p_input2, sum;
  
  //input first number

  printf("Please enter the first number to be added: ");
  
  number_of_conversions = scanf("%d", &input1);
  while(number_of_conversions != 1)
    {
      clear_key();
      printf("Please enter an integer.\n");
      number_of_conversions = scanf("%d", &input1);
    }

  //input second number
  printf("Please enter the second  number to be added: ");
  number_of_conversions = scanf("%d", &input2);
  while(number_of_conversions !=1)
    {
      clear_key();
      printf("Please enter an integer. \n");
      number_of_conversions = scanf("%d", &input2);

    }

  // add number using pointers 
  p_input1 = &input1;
  p_input2= &input2;

  sum = *(p_input1) + *(p_input2);

  printf("The sum of the two numbers is %d\n", sum);

  return 0;
  
  
}
