/*
Write a Program to find the largest element using Dynamic Memory Allocation

I am going to add an RNG seeded using time to seed a user defined array. This will then find the largest element and print it out.


I downloaded Valgrind to check to make sure that I free all the memory.
syntax: valgrind --leak-check=yes 'program arg1 arg2'
 */


#include <stdio.h>
#include <stdlib.h> //for malloc
#include <time.h> //to seed the RNG
//fill array with random numbers
void fill_array(int* array_to_be_filled, int array_size);

//scan for largest integer in the array
int find_largest_element(int* array_to_scan, int array_size);

int main()
{

  int* p_int_array;    
  int array_size, i;
  //get a user input
  printf("Please enter the number of elements to be added: ");
  scanf("%d", &array_size);
  
  //allocate space
  p_int_array = (int*)malloc(sizeof(int) * array_size);
  if(p_int_array == NULL)
    {
      printf("Error allocating memory!\n");
      return(-1);
    }
  //fill array

  fill_array(p_int_array, array_size);
  
  //find largest element
  printf("There are %d elements in the array. The elements are: \n", array_size);
  for(i = 0; i < array_size; i++)
    {
      printf("Element %d | Content %d \n", i, p_int_array[i]);
    }

  printf("The largest element in the array is: %d.\n", find_largest_element(p_int_array, array_size));

  

  free(p_int_array);
  return 0;
  
}

void fill_array(int* array_to_be_filled, int array_size)
{
  srand(time(NULL));
 
  int i;
  for(i = 0; i < array_size; i++)
    {
      array_to_be_filled[i] = rand();
    }
 }

int find_largest_element(int* array_to_scan, int array_size)
{
  int i, largest;

  largest = array_to_scan[0];
  for(i = 1; i < array_size; i++)
    {
      if(largest <= array_to_scan[i])
	{
	  largest = array_to_scan[i];
	}

    }

  return largest;
  
}


