#include<stdio.h>



int main()
{
int m,n,o,i;
int* z; 

printf("Pointer : Show the basic declaration of pointer: \n");

for(i=0; i <= 50; i++)
{
printf("-");
if(i == 50)
	printf("\n");
}

m = 10;
printf("here m = 10, n and o are two integer variable, and *z is an integer \n");

z = &m; 

printf("z stores the address of m = %p \n", z); // %p allows the printing of pointers.

printf("*z stores the address of m = %d \n", *z);

printf("&m is the address of m = %p \n", &m);

printf("&n stores the address of n = %p \n", &n);

printf("&o stores the address of o = %p \n", &o);

printf("&z stores the address of z = %p \n", &z);

return 0;


}