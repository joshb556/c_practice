// Add two numbers using call by reference

#include<stdio.h>



// this function will take two addresses and sum them. it will return an integer //that is the sum
int add_2_numbers(int* number1, int* number2);

int main()
{
  int input1, input2;
  //take input for the two numbers to be summed
  
  printf("Enter the first integer to be summed.\n");
  scanf("%d", &input1);

  printf("Enter the second integer to be summed. \n");
  scanf("%d", &input2);

  // call the add function and print sum
  printf("The sum is : %d \n", add_2_numbers(&input1, &input2));

	 return 0;
  
}


int add_2_numbers(int* number1, int* number2)
{
  
  int sum;
  // add both numbers
  sum = *number1 + *number2;
  //return the sum
    return sum;
  
}
