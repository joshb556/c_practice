#include <stdio.h>

int main()
{
  int m = 29;
  int* ab;
  
  // display address and value of m

  printf("The address of m is %p \n", &m);
  printf("The value of m is %d \n", m);

  //display address and content  of ab and assign to address of m
  ab = &m;

  printf("Now ab is assigned with the address of m.\n");
  printf("The address of pointer ab is %p \n", &ab);
  printf("The content of pointer ab is %d \n", *ab);
  
  
  // assign m a value of 34 and display address of ab and content of ab
  printf("The value of m is assigned to 34 now.\n");
  m = 34;
  printf("Address of pointer ab: %p.\n", &ab);
  printf("Content of pointer ab: %d \n", *ab);
  
  
  // assign ab value of 7 and display the address and value of m

  printf("The pointer variable ab is assigned with the value of 7. \n");
  *ab = 7;

  printf("Address of m : %p. \n", &m);
  printf("value of m : %d. \n", m);

  return 0;
  
}
