
// practice with malloc
// the syntax of malloc is as follows: pointer = (type*)malloc(size)
// malloc gets a chunk of memory and assigns it to a variable. sets aside
// memory the size of 'size' and returns a pointer to the beginning of the chunk
// or NULL (if memory allocation fails). (type*) specifies the type that the memory is being set aside for.

#include <stdio.h>
#include <stdlib.h>

int main()
{
  int* p_int_array;
  int array_size, i, input, n;

  printf("Please enter the size of the array: ");
  scanf("%d", &array_size);
  
  p_int_array =(int*) malloc(sizeof(int)*array_size);
  if(p_int_array == NULL)
    {
      printf("FAILED TO ALLOCATE MEMORY.\n");
      return(-1);

    }

  for(i = 0; i < array_size; i++)
    {

      printf("enter the number in index%d: ", i+1);
      //scanf("%d", &input);
      printf("\n");
      p_int_array[i] = 1;
    }

  for(i = 0; i<array_size; i++)
    {
      printf("index: %d | content: %d\n", i, p_int_array[i]);
     
      

    }
  ;

  printf("%lu", sizeof(p_int_array)/sizeof(int));
  
  


  free(p_int_array);
  return 0;
  
}
