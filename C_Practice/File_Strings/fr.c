/*
this is a program to read in from a text file and put it in a character array of the 
appropriate size

Simple program to open a file and print a string to it.
File is called File.txt

char* fgets(char* string, int n, FILE* stream)
string = pointer to an array of characters where string is stored/read
n  = the maximum number of characters to be read (including NULL)
stream = the file that is being read from

int fprintf(FILE* stream, const char* format, ...)
stream = file to print to
format = c string containing text to be written

FILE* fopen(const char* filename, const char* mode);
filename = the char array holding the files name;
mode = what mode this is going to be in: "w" is write, "r", is read, "r+" is both




*/
#include <stdio.h>
#include<stdlib.h>

int get_char_count(FILE* p_file);

int main()
    {
        FILE* p_file;
        char file_name[20] = "test.txt";
        char c;
        int count;
        int i;
        char* file_buffer;

        p_file = fopen(file_name, "r");

        if(p_file == NULL)
            {
            printf("Failed to open the file!\n");
            exit(1);
            }
        count = get_char_count(p_file);


        file_buffer = (char*)calloc(count+1, sizeof(char));

        for(i = 0; i < count-1; i++)
            {
                file_buffer[i] = fgetc(p_file);
            }
        rewind(p_file);

        for(i=0; c != '\0'; i++)
            {
                c = file_buffer[i];
            }

        printf("count: %d | scan size: %d\n", count, i);
        printf("%d\n", count);
        printf("%s\n", file_buffer);

        fclose(p_file);
        free(file_buffer);
        return 0;
    }

int get_char_count(FILE* p_file)
    {
        int i;
        char c;
        for(i = 0; !feof(p_file); i++)
        {
        c = fgetc(p_file);
        }
        rewind(p_file);
        
    
        return i;
    }

