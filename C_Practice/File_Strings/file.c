/*
Simple program to open a file and print a string to it.
File is called File.txt

char* fgets(char* string, int n, FILE* stream)
string = pointer to an array of characters where string is stored/read
n  = the maximum number of characters to be read (including NULL)
stream = the file that is being read from

int fprintf(FILE* stream, const char* format, ...)
stream = file to print to
format = c string containing text to be written



*/
#include<stdio.h>
#include<stdlib.h>
#include <string.h>


int main()
{
    char string[1000]; //string input
    char string2[1000];
FILE* p_file; // file pointer
char file_name[20] = "test.txt"; //file name

p_file = fopen(file_name, "r+"); //opens file, p_file now points to it
if(p_file == NULL)
{
    printf("Failed to open file. Exiting.\n");
    exit(1);
}

fgets(string, sizeof(string), stdin); //<getting input from stdin
fprintf(p_file, "%s", string);

fclose(p_file); // close the file
return 0;

}