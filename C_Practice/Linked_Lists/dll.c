/*
practice with a doubley linked list
 */
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
  int data;
  struct node* next_node;
  struct node* previous_node;
  struct node* tail_node;
}node;


typedef struct node* p_node;

void push(p_node* head_node, int data);
int list_length(p_node head_node);
void print_list(p_node head_node);

int main()
{
  p_node my_list = NULL;
  print_list(my_list);
  push(&my_list, 5);
  print_list(my_list);
  push(&my_list, 25);
  push(&my_list, 32);
  print_list(my_list);


  return 0;
}


int list_length(p_node head_node)
{
  p_node current_node = head_node;
  int count = 0;
  while(current_node != NULL)
    {
      current_node = current_node->next_node;
      count++;
    }
  return count;
}


void push(p_node* head_node, int data)
{
  p_node new_node = (p_node)malloc(sizeof(node));

  new_node ->data = data;
  new_node->previous_node = NULL;
  new_node->next_node = *head_node;
  if(*head_node != NULL)
    {
  new_node->next_node->previous_node = new_node;
  new_node->tail_node = new_node->next_node->tail_node;
    }else if(*head_node == NULL)
    {
      new_node ->tail_node = new_node;
	}
  *head_node = new_node;
 
  
}

void print_list(p_node head_node)
{
  int i;
  p_node current_node = head_node;
  printf("Printing List: \n");
   if(head_node == NULL)
	{
	  printf("The list is empty!\n");
	 
	}

  for(i = 0; current_node != NULL; i++)
    {
      if(current_node-> previous_node ==NULL && current_node->next_node ==NULL)
	{
	  printf("node %d: NULL <-prev |Data in node: %d| Data in tail node: %d| -> next NULL \n",i, current_node->data,current_node->tail_node->data);
	    
	}
      else if(current_node->previous_node == NULL)
	{
	  printf("node %d: NULL <-prev |Data in node: %d| Data in tail node: %d| -> next %d \n",i, current_node->data, current_node->tail_node->data, current_node->next_node->data);
	    
	    }else if(current_node->next_node == NULL)
	{
	  printf("node %d: %d <-prev |Data in node: %d| Data in tail node: %d| -> next NULL \n",i,current_node->previous_node->data, current_node->data, current_node->tail_node->data);
	    
	     }else{
	printf("node %d: %d <-prev |Data in node: %d| Data in tail node: %d| -> next %d \n",i,current_node->previous_node->data, current_node->data, current_node->tail_node->data, current_node->next_node->data);
	 }
      current_node = current_node->next_node;
	 }
}
