/*
third and final attempt at linked list
 */
#include<stdio.h>
#include<stdlib.h>

typedef enum BOOL{FALSE,
		  TRUE
}BOOL;
  
typedef struct node{
  int data;
  struct node* next_node;
}node;

typedef struct node* p_node;

void push_node(p_node* head_node,int data);
void push_node_optimized(p_node* head_node, p_node* tail_node, int data);
void append_node(p_node* head_node, int data);
void destroy_list(p_node* head_node);
int count_list(p_node head_node);
int pop(p_node *head_node);
BOOL is_empty(p_node head_node);

int main()
{
  p_node my_list = NULL;
  p_node tail;
 printf("%d\n", pop(&my_list));  
	 
 printf("before append: %d\n",count_list(my_list));
  
  append_node(&my_list,25);
  printf("%d\n", my_list->data);
  printf("after append: %d\n",count_list(my_list));  
  printf("pop %d\n", pop(&my_list));
  printf("after pop: %d\n",count_list(my_list));
  
  push_node(&my_list, 5);
  printf("%d\n", my_list->data);
  printf("after push: %d\n",count_list(my_list));
  
  destroy_list(&my_list);
printf("after destroy: %d\n",count_list(my_list));
  return 0;
}

void push_node_optimized(p_node* head_node, p_node* tail_node, int data)
{
  p_node new_node;

  new_node = (p_node)malloc(sizeof(node));
  if(new_node == NULL)
    {
      printf("Allocation failed!\n");
      exit(1);
    }
  new_node-> data = data;
  new_node->next_node = *head_node;
  *head_node = new_node;
  if(new_node->next_node == NULL)
    {
      *tail_node = new_node;
    }  

}

void push_node(p_node* head_node, int data)
{
  p_node new_node;

  new_node = (p_node)malloc(sizeof(node));
  if(new_node == NULL)
    {
      printf("Allocation failed!\n");
      exit(1);
    }
  new_node-> data = data;
  new_node->next_node = *head_node;
  *head_node = new_node;
  
}


void append_node(p_node* head_node, int data)
{
  p_node current_node = *head_node; 
  p_node new_node = (p_node)malloc(sizeof(node));
    if(new_node == NULL)
    {
      printf("Allocation failed!\n");
      exit(1);
    }
  new_node->data = data;
  new_node->next_node = NULL;

  if(current_node == NULL)
    {
      *head_node = new_node;
    }else{
  while(current_node->next_node != NULL)
    {
      current_node = current_node->next_node;           
    }
  current_node->next_node = new_node;
  }
}

void destroy_list(p_node* head_node)
{
  p_node current_node = *head_node;
  p_node next_node;
  while(current_node != NULL)
    {
      next_node = current_node->next_node;
      free(current_node);
      current_node=next_node;
    }
  *head_node = NULL;  
}

int count_list(p_node head_node)
{
  p_node current_node = head_node;
  int count = 0;
  while(current_node != NULL)
    {
      current_node = current_node->next_node;
      count++;
    }

  return count;
  
}

BOOL is_empty(p_node head_node)
{
 BOOL is_empty;

  if(head_node == NULL)
    {
      is_empty = TRUE;
    }
  else if(head_node != NULL)
    {
      is_empty = FALSE;
    }
  else
    {
      printf("This should never be seen.\n");
      exit(1);
    }

  return is_empty;
}

int pop(p_node* head_node)
{
  int node_data;
  p_node current_node = *head_node;
  p_node temp;
  if(is_empty(current_node))
    {
      printf("The stack is empty!returning : ");
      return NULL;
    }
  else if(current_node->next_node == NULL)
    {
      node_data = current_node->data;
free(*head_node);
      *head_node = NULL;

      
    }
  else
    {
      node_data = current_node->data;
      temp = current_node->next_node;
      free(*head_node);
      *head_node = temp;
      free(temp);
    }



  
  return node_data;
  
}

