// this is my second attempt to make a linked list ADT using the standford study guide

// I almost have this down. My problem is that I have the condition that relies
// on node->next_node != NULL. I need to work on setting empty nodes as NULL and
// and handling NULL nodes.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct node{
  int data;
  struct node* next_node;
}node;

typedef struct node* p_node; //p_node is a pointer to a node

p_node create_list();
p_node initialize_list(p_node* list);

void push_node(p_node* head_node, int data);
void print_list(p_node head_node);
int list_length(p_node head_node);
void destroy_list(p_node* head_node);
void append_node(p_node* head_node, int data);

int main()
{
  int data1 = 0;
  int data2 = 99;
  p_node my_list  = NULL;  

  append_node(&my_list, 50);
  
  print_list(my_list);

  //printf("%d\n", list_length(my_list));

 
  
  append_node(&my_list, data2);
  push_node(&my_list, data1);
  print_list(my_list);

  destroy_list(&my_list);

  return 0;  
}



//creates a list of user defined size and fills with a random number
p_node create_list()
{
  srand(time(NULL));
  
  p_node head_node=NULL;
  int list_size, i;
  printf("How big? : ");
  scanf("%d", &list_size);
  printf("\n");   
  for(i = 0; i < list_size; i++)
    {        
     push_node(&head_node, rand());      
    } 
  return head_node;  
}



int list_length(p_node head_node)
{
  p_node current = head_node;
  int count = 1;

  while(current->next_node != NULL)
    {
      count++;
      current = current-> next_node;
    }
  return count;  
}



void push_node(p_node* head_node, int data)
{
  p_node new_node = (p_node)malloc(sizeof(node));
  if(new_node == NULL)
    {
      printf("FAILURE ALLOCATING MEMORY DURING NODE PUSH!\n");	     

    }

  new_node->data = data;
  new_node->next_node = *head_node;
  *head_node = new_node;  

}

void append_node(p_node* head_node, int data)
{
  srand(time(NULL));
  p_node current_node = *head_node;
  p_node new_node;

  new_node = (p_node)malloc(sizeof(node));
  new_node->data = data;
  new_node->next_node = NULL;
  
  if(*head_node == NULL)
    {
      *head_node = new_node;     
    }

  else{
  while(current_node->next_node != NULL)
    {
      current_node = current_node -> next_node;     
	
    }
  current_node->next_node = new_node;
  }
}



void print_list(p_node head_node)
{
  p_node current_node = head_node;
  int i;

  for(i = 0; current_node->next_node !=  NULL ||  current_node != NULL; i++)
    {
      printf("Data in node %d | %d\n", i+1, current_node->data);      
      current_node = current_node->next_node;
      
      if(current_node->next_node == NULL)
	{
         printf("Data in node %d | %d\n", i+2, current_node->data);      
	}
    }

  printf("Total nodes : %d \n", list_length(head_node));  
}




void destroy_list(p_node* head_node)
{
  p_node current_node = *head_node;
  p_node next_node;

  while(current_node->next_node != NULL)
    {
      next_node = current_node->next_node;
      free(current_node);
      current_node = next_node;    
    }
  free(current_node);
}

