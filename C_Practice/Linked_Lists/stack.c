/*
This is my implementation of a stack using a singly linked list.

This will have the following functions:


*push : pushes new data to the head node

pop : removes head node and returns the data

*destroy : destroys the stack and frees all the memory

*stack_size : returns the number of nodes in the stack

*is_empty : returns true or false if the stack is empty or not

*append : this is an optimized version of append. I am keeping track of a tail pointer

*print_stack : prints stack contents

'*' indicates finished functions

I may add features to have user input:

set_stack_size
enter_data


 */
#include<stdio.h>
#include<stdlib.h>

//the tail_node pointer is kept to allow for appending in O(1) time.
typedef struct node{
  int data;
  struct node* next_node;
  struct node* tail_node;
}node;



typedef struct node* p_node;

typedef enum BOOL
  {
    FALSE,
    TRUE
  }BOOL;

BOOL is_empty(p_node head_node);
void push(p_node* head_node, int data);
int stack_size(p_node head_node);
void print_stack(p_node head_node);
void destroy_stack(p_node* head_node);
void append_node(p_node* head_node, int data);



int main()
{
  int i;
  p_node my_list=NULL; // this should give the address of the head

  print_stack(my_list);
  push(&my_list, 5);
  push(&my_list, 25);
  for(i = 0; i < 5000; i++)
    {
  push(&my_list, 32);
    }
  //print_stack(my_list);
  append_node(&my_list, 99);
  //print_stack(my_list);
  append_node(&my_list, 42);
  print_stack(my_list);
  destroy_stack(&my_list);
}


// replace head with new node. new node is now head.
void push(p_node* head_node, int data)
{
  p_node new_node = (p_node)malloc(sizeof(node));
  p_node temp = *head_node;
  if(temp == NULL)
    {
      printf("Creating new list!\n");
      new_node->tail_node = new_node;
    }
  else{
    new_node->tail_node = temp->tail_node;
  }

    
  
  new_node-> data = data;
  new_node->next_node = *head_node;
  
  *head_node = new_node;  

  
    
}

//iterate through the stack and return the number of nodes.
int stack_size(p_node head_node)
{
  p_node current_node = head_node;
  int count = 0;

  while(current_node != NULL)
    {
      current_node = current_node->next_node;
      count++;
    }

  return count;
}

//print the data stored in each node
void print_stack(p_node head_node)
{
  int count = 0;
  p_node current_node= head_node;
  
  if(is_empty(current_node))
    {
      printf("The stack is empty!.\n");
      return;
    }
  else{
  while(current_node != NULL)
    {
      printf("Data in node %d | %d\n", count, current_node->data);
      count++;
      current_node = current_node->next_node;
    }
  printf("The tail node has %d in its data.\n", head_node->tail_node->data);
    }
}
//check to see if the stack is emtpy or not. returns true or false.
  BOOL is_empty(p_node head_node)
  {
    BOOL is_empty;
    if(head_node == NULL)
      {
	is_empty = TRUE;
      }
    else if(head_node != NULL)
      {
	is_empty = FALSE;
      }
    else
      {
	printf("If you are seeing this. Something went really wrong.\n");
      }
    return is_empty;
      }
    
void destroy_stack(p_node* head_node)
{
  p_node current_node = *head_node;
  p_node next_node;
  while(current_node != NULL)
    {
      next_node = current_node->next_node;
      free(current_node);
      current_node = next_node;
    }
  *head_node = NULL;
  free(current_node);
  if(is_empty(current_node))
    {
      printf("Stack destruction success!\n");
    }
}

void append_node(p_node* head_node, int data)
{
  p_node new_node = (p_node)malloc(sizeof(node));
  p_node current_node = *head_node;
  p_node head = *head_node;
  current_node = current_node->tail_node;
  current_node-> next_node = new_node;
  new_node->data = data;
  new_node->tail_node = new_node;
  head->tail_node = new_node;
}
