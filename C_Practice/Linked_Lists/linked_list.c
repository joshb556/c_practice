/*
practice creating basic linked list & nodes

 */

#include<stdio.h>
#include<stdlib.h>


typedef struct node
{
  int data;
  struct node* next_node;  
  
}NODE;

//node type now becomes the type "linked_list pointer"
typedef struct node* p_node;

//this function will return pointer to the new node
p_node create_node();
void add_node(p_node node_head); // adds a new node
void destroy_list(p_node node_head); //destroys all the nodes
int list_length(p_node node_head); // counts the legth of the list. returns length


int main()
{
  p_node my_node;
  int i;

  my_node = create_node();
  
  my_node->data = 12;

  printf("%d\n", my_node->data);

  add_node(my_node);
  


 
  
  for(i = 0; i < 3; i++)
    {
      add_node(my_node);

    }

  
  printf("%d\n", list_length(my_node));
  destroy_list(my_node);



  
  
  //free(my_node);
  
  return 0;  
}


p_node create_node()
{
  p_node temp;
  temp = (p_node)malloc(sizeof(NODE)); //allocating memory. temp now is a pointer to a new node.
  if(temp == NULL)
    {
      printf("failed to allocate memory!\n");      
    }
  temp->next_node = NULL; // since this is a new node being added (presumebly at the end) the next should be NULL
 
  return temp;    
}

void add_node(p_node node_head)
{
  p_node temp = node_head;
  while(temp->next_node != NULL)
    {
      temp = temp->next_node;
    }
  temp->next_node = create_node();
  }

int list_length(p_node node_head)
{
  int count=1;
  p_node temp = node_head;

  while(temp->next_node != NULL)
    {
      count++;
      temp = temp->next_node;
    }
  
  return count;
}



void destroy_list(p_node node_head)
{
  p_node temp, next;

  temp = node_head;

  while(temp->next_node != NULL)
    {
      next = temp->next_node;
      free(temp);
      temp = next;
    }
  free(temp);
}


