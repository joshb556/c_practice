/*
practice with a doubley linked list This will be a header file for a doubly linked list
 */
#include<stdio.h>
#include<stdlib.h>

typedef enum face{ 
    ACE, TWO, THREE, 
    FOUR, FIVE, SIX, 
    SEVEN, EIGHT, NINE, 
    TEN, JACK, QUEEN, 
    KING
} FACE;

typedef enum suit {
HEARTS, DIAMONDS, SPADES, CLUBS
}SUIT;

typedef struct node{
  int position_in_deck;
  int data;
  FACE card_face;
  SUIT card_suit;
  char face[10];
  char suit[10];
  struct node* next_node;
  struct node* previous_node;
  struct node* tail_node;
}node;


typedef struct node* p_node;

void initialize_deck(p_node* head_node, int data);
void push(p_node* head_node, int data);
int list_length(p_node head_node);
void print_list(p_node head_node);

/*int main()
{
  p_node my_list = NULL;
  print_list(my_list);
  push(&my_list, 5);
  print_list(my_list);
  push(&my_list, 25);
  push(&my_list, 32);
  print_list(my_list);


  return 0;
}*/


// Function Definitions
void initialize_deck(p_node* head_node, int data)
{
  p_node current_node = *head_node;
  for(i = 0; i < 52; i++)
  {
  push(&current_node, data);
  }
 *head_node = current_node;
}

int list_length(p_node head_node)
{
  p_node current_node = head_node;
  int count = 0;
  while(current_node != NULL)
    {
      current_node = current_node->next_node;
      count++;
    }
  return count;
}

void push(p_node* head_node, int data)
{
  p_node new_node = (p_node)malloc(sizeof(node));

  new_node ->data = data;
  new_node->previous_node = NULL;
  new_node->next_node = *head_node;
   switch(new_node->data)
  {    
    case 0 :
    new_node->card_face = ACE;
    break;
    case 1:
    new_node->card_face = TWO;
    break;
    case 2:
    new_node->card_face = THREE;
    break;
    case 3:
    new_node->card_face = FOUR;
    break;
    case 4:
   new_node->card_face = FIVE;
    break;
    case 5:
    new_node->card_face = SIX;
    break;
    case 6:
    new_node->card_face = SEVEN;
    break;
    case 7:
    new_node->card_face = EIGHT;
    break;
    case 8:
    new_node ->card_face = NINE;
    break;
    case 9:
    new_node->card_face = TEN;
    break;
    case 10:
   new_node->card_face = JACK;
    break;
    case 11:
    new_node->card_face = QUEEN;
    break;
    case 12:
    new_node->card_face = KING;
    break;
  };
  if(*head_node != NULL)
    {
  new_node->next_node->previous_node = new_node;
  new_node->tail_node = new_node->next_node->tail_node;
    }else if(*head_node == NULL)
    {
      new_node ->tail_node = new_node;
	}

  *head_node = new_node;
 
  
}

void print_list(p_node head_node)
{
  
  int i;

  p_node current_node = head_node;
  printf("Printing List: \n");
   if(head_node == NULL)
	{
	  printf("The list is empty!\n");
	 
	}

  switch(head_node->card_face)
    {
      case ACE :  
      printf("Ace\n");
      break;  

      case TWO :   
        printf("Two\n");
        break; 

      case THREE :   
        printf("Three\n");
        break;

      case FOUR :     
      printf("Four\n");
        break;

      case FIVE :   
      printf("Five\n");
        break;

      case SIX :     
      printf("Six\n");
      break;

      case SEVEN : 
      printf("Seven\n");
      break;

      case EIGHT : 
      printf("Eight\n");
      break;

      case NINE :
        printf("Nine\n");
        break;

      case TEN : 
      printf("Ten\n");
      break;

      case JACK : 
      printf("Jack\n");
      break;

      case QUEEN :
      printf("Queen\n");
      break;

      case KING :
      printf("King\n");
      break;
    }
  


  for(i = 0; current_node != NULL; i++)
    {
      if(current_node-> previous_node ==NULL && current_node->next_node ==NULL)
	{
	  printf("node %d: NULL <-prev |Data in node: %d| Data in tail node: %d| -> next NULL \n",i, current_node->data,current_node->tail_node->data);
	    
	}
      else if(current_node->previous_node == NULL)
	{
	  printf("node %d: NULL <-prev |Data in node: %d| Data in tail node: %d| -> next %d \n",i, current_node->data, current_node->tail_node->data, current_node->next_node->data);
	    
	    }else if(current_node->next_node == NULL)
	{
	  printf("node %d: %d <-prev |Data in node: %d| Data in tail node: %d| -> next NULL \n",i,current_node->previous_node->data, current_node->data, current_node->tail_node->data);
	    
	     }else{
	printf("node %d: %d <-prev |Data in node: %d| Data in tail node: %d| -> next %d \n",i,current_node->previous_node->data, current_node->data, current_node->tail_node->data, current_node->next_node->data);
	 }
      current_node = current_node->next_node;
	 }
  
}

void destroy_list(p_node* head_node)
  {
    p_node current_node = *head_node;
    p_node next_node;

    while(current_node->next_node != NULL)
      {
        next_node = current_node->next_node;
        free(current_node);
        current_node = next_node;    
      }
    free(current_node);
  }
